﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FMod
{
    public class InterstitialAdsLauncher : MonoBehaviour
    {
        public void TryShow()
        {
            InterstitialAds.ShowQuick(null);
        }
    }

}