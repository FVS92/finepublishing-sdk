﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FMod
{
    public static class AdsAnalytics
    {
        static int interstitialAdsCount = 0;

        public static void LogShowInterstitial()
        {
            ///
            interstitialAdsCount++;

            ///
            Analytics.LogEvent("ShowInterAds", "inSessionCount", interstitialAdsCount, 1);
        }

        public static void LogShowBanner(BannerAdsPosition bannerAdsPosition)
        {
            switch (bannerAdsPosition)
            {
                case BannerAdsPosition.Top:
                    Analytics.LogEvent("ShowBannerAds_Top", 1);
                    break;
                case BannerAdsPosition.Bottom:
                    Analytics.LogEvent("ShowBannerAds_Bottom", 1);
                    break;
                default:
                    break;
            }
        }

        public static void LogShowRewarded()
        {
            Analytics.LogEvent("ShowRewardedAds", 1);
        }

        public static void LogCompletedRewarded(string adId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                { "AdId", adId },
                { "LifeTimeCount", Ads.RewardedAdsCompletedCount }
            };

            ///
            Analytics.LogEvent("CompletedRewardedAds", 1, parameters);
        }

    }

}