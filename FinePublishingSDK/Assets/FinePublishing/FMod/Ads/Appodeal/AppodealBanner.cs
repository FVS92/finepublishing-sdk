﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

namespace FMod
{
    public class AppodealBanner : MonoBehaviour, IBannerAdListener, IBannerAds
    {
        const float RefreshInterval = 10.0f;

        bool showing = false;
        BannerAdsPosition position;

        bool isLoaded = false;

        public event BannerAvailabilityChangedHandler OnAvailabilityChanged;

        public string AdId => AdsIds.AppodealBanner;

        public bool IsAvailable { get; private set; }
        public int ListId { get; set; }

        public void Init()
        {
            ///
            isLoaded = Appodeal.isLoaded(Appodeal.BANNER);
            IsAvailable = isLoaded;

            ///
            Appodeal.setBannerCallbacks(this);
        }

        public void Show(BannerAdsPosition position)
        {
            ///
            Debug.LogFormat("Try to show Appodeal banner, {0}, {1}, {2}", position, gameObject.GetInstanceID(), Time.unscaledTime);

            ///
            this.position = position;
            showing = true;

            ///
            if (!isLoaded)
            {
                return;
            }

            ///
            int bannerPos;

            ///
            switch (position)
            {
                case BannerAdsPosition.Top:
                    bannerPos = Appodeal.BANNER_TOP;
                    break;
                case BannerAdsPosition.Bottom:
                    bannerPos = Appodeal.BANNER_BOTTOM;
                    break;
                default:
                    throw new System.NotImplementedException();
            }

            ///
            Appodeal.show(bannerPos);

            ///
            Debug.LogFormat("Show Appodeal banner, {0}", bannerPos);
        }

        public void Hide()
        {
            showing = false;
            Appodeal.hide(Appodeal.BANNER);
        }

        #region Banner callbacks
        public void onBannerLoaded(bool isPrecache)
        {
            ///
            isLoaded = true;
            IsAvailable = true;

            ///
            OnAvailabilityChanged?.Invoke(this);

            ///
            if (showing)
            {
                ///
                Show(position);
            }

            ///
            Debug.LogFormat("Appodeal, onBannerLoaded, {0}, {1}", gameObject.GetInstanceID(), Time.unscaledTime);
        }

        public void onBannerFailedToLoad()
        {
            ///
            IsAvailable = false;

            ///
            OnAvailabilityChanged?.Invoke(this);

            ///
            Debug.Log("Appodeal, BannerFailedToLoad");
        }

        public void onBannerShown()
        {
            ///
            Debug.Log("Appodeal, onBannerShown");
        }

        public void onBannerClicked()
        {
            Debug.Log("Appodeal, onBannerClicked");
        }

        public void onBannerExpired()
        {
            Debug.Log("Appodeal, BannerExpired");
        }

        public void onBannerLoaded(int height, bool isPrecache)
        {
            ///
            isLoaded = true;
            IsAvailable = true;

            ///
            OnAvailabilityChanged?.Invoke(this);

            ///
            if (showing)
            {
                ///
                Show(position);
            }

            ///
            Debug.LogFormat("Appodeal, onBannerLoaded, {0}, {1}", gameObject.GetInstanceID(), Time.unscaledTime);
        }
        #endregion
    }

}