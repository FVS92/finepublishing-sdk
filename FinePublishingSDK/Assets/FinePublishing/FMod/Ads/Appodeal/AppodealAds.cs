﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace FMod
{
    public class AppodealAds : MonoBehaviour
    {
        public static AppodealAds Instance { get; private set; }

        [SerializeField]
        GameObject adsObjectsRoot;

        [Space]
        [SerializeField]
        MString appKey;

        [Space]
        [SerializeField]
        AppodealInterstitial interstitial;
        [SerializeField]
        AppodealRewarded rewarded;
        [SerializeField]
        AppodealBanner banner;

        public AppodealInterstitial Interstitial
        {
            get
            {
                return interstitial;
            }
        }

        public AppodealRewarded Rewarded
        {
            get
            {
                return rewarded;
            }
        }

        public AppodealBanner Banner
        {
            get
            {
                return banner;
            }
        }

        public void Awake()
        {
#if !UNITY_STANDALONE
            ///
            if (Instance != null)
            {
                DestroyImmediate(gameObject);
                return;
            }

            ///
            Instance = this;
            DontDestroyOnLoad(gameObject);

            ///
            Init();

            ///
            adsObjectsRoot.SetActive(true);
#else
            Destroy(gameObject);
#endif
        }

        void Init()
        {
            ///
            Debug.LogFormat("Appodeal plugin version: {0}", Appodeal.getPluginVersion());

            ///
            Appodeal.disableLocationPermissionCheck();
            Appodeal.disableWriteExternalStoragePermissionCheck();
            
            ///                        
            if (Ads.EnableInterAndBanner)
            {
                Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW | Appodeal.REWARDED_VIDEO | Appodeal.BANNER, GDPRManager.IsConsent);
                Debug.Log("Appodeal, Inited full");
            }
            else
            {
                Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO, GDPRManager.IsConsent);
                Debug.Log("Appodeal, REWARDED_VIDEO only");
            }

            ///
            Appodeal.setSmartBanners(true);

            ///
            if (Interstitial != null)
            {
                Interstitial.Init();
            }
            if (Rewarded != null)
            {
                Rewarded.Init();
            }
            if (Banner != null)
            {
                Banner.Init();
            }
        }
    }

}