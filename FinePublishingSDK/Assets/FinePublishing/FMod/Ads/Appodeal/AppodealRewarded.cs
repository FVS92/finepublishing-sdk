﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace FMod
{
    public class AppodealRewarded : MonoBehaviour, IRewardedVideoAdListener, IRewardedAds
    {
        RewardedAdsClosedCallback onClosedCallback;

        public string AdId => AdsIds.AppodealRewarded;

        public bool IsAvailable
        {
            get
            {
                return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
            }
        }

        public bool IsMonetizable => true;

        public void Init()
        {
            Appodeal.setRewardedVideoCallbacks(this);
        }

        public void Show(RewardedAdsClosedCallback onClosedCallback)
        {
            ///
            this.onClosedCallback = onClosedCallback;

            ///
            Appodeal.show(Appodeal.REWARDED_VIDEO);
        }

        #region Rewarded callbacks
        public void onRewardedVideoLoaded()
        {

        }

        public void onRewardedVideoFailedToLoad()
        {
            Debug.Log("Appodeal, RewardedVideoFailedToLoad");
        }

        public void onRewardedVideoShown()
        {

        }

        public void onRewardedVideoFinished(int amount, string name)
        {

        }

        public void onRewardedVideoClosed(bool finished)
        {
            if (onClosedCallback != null)
            {
                onClosedCallback(finished);
            }
        }

        public void onRewardedVideoLoaded(bool precache)
        {
            //   throw new System.NotImplementedException();
        }

        public void onRewardedVideoFinished(double amount, string name)
        {
            // throw new System.NotImplementedException();
        }

        public void onRewardedVideoExpired()
        {
            Debug.Log("Appodeal, RewardedVideoExpired");
        }

        public void onRewardedVideoClicked()
        {

        }

        public void onRewardedVideoShowFailed()
        {
            Debug.Log("Appodeal, onRewardedVideoShowFailed");
        }
        #endregion
    }

}