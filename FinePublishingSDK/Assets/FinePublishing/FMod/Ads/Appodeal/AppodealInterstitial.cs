﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

namespace FMod
{
    public class AppodealInterstitial : MonoBehaviour, IInterstitialAdListener, IInterstitialAds
    {
        private const string ShowCountKey = "AppodealInterCount";
        private const string MinStaticAdsCountKey = "AppodealInterMinStatic";
        private const string StaticAdsPlacementKey = "AppodealInterStaticAdsPlacement";

        [SerializeField]
        private string defaultStaticAdsPlacement="StaticInter";
        [SerializeField]
        private int defaultMinStaticAdsCount = 1;

        private string staticAdsPlacement;
        private int minStaticAdsCount;

        private int ShowCount
        {
            get
            {
                return PlayerPrefs.GetInt(ShowCountKey);
            }

            set
            {
                PlayerPrefs.SetInt(ShowCountKey, value);
            }
        }

        public bool IsAvailable
        {
            get
            {
                return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
            }
        }

        Action onClosedCallback = null;

        public string AdId => AdsIds.AppodealInterstitial;

        public void Init()
        {
            ///
            Appodeal.setInterstitialCallbacks(this);

            ///
            GetRemoteValue();
        }

        public void Show(Action onClosedCallback)
        {
            ///
            this.onClosedCallback = onClosedCallback;

            ///
            if (ShowCount > minStaticAdsCount)
            {
                Appodeal.show(Appodeal.INTERSTITIAL);
            }
            else
            {
                if (!Appodeal.show(Appodeal.INTERSTITIAL, staticAdsPlacement))
                {
                    Appodeal.show(Appodeal.INTERSTITIAL);
                }
            }
        }

        private void GetRemoteValue()
        {
            staticAdsPlacement = RemoteSettings.GetString(StaticAdsPlacementKey, defaultStaticAdsPlacement);
            minStaticAdsCount = RemoteSettings.GetInt(MinStaticAdsCountKey, defaultMinStaticAdsCount);
        }

        #region Interstitial callbacks
        public void onInterstitialLoaded(bool isPrecache)
        {

        }

        public void onInterstitialFailedToLoad()
        {
            Debug.Log("Appodeal, InterstitialFailedToLoad");
        }

        public void onInterstitialShown()
        {
            ShowCount++;
        }

        public void onInterstitialClosed()
        {
            if (onClosedCallback != null)
            {
                onClosedCallback();
            }
        }

        public void onInterstitialClicked()
        {

        }

        public void onInterstitialExpired()
        {
            Debug.Log("Appodeal, InterstitialExpired");
        }

        public void onInterstitialShowFailed()
        {
            Debug.Log("Appodeal, onInterstitialShowFailed");
        }

        #endregion
    }

}