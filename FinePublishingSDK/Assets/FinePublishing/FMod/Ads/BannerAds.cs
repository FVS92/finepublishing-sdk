﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FMod
{
    public static class BannerAds
    {
        static List<IBannerAds> allBannerAds;

        static IBannerAds activeBannerAds;

        static bool inited = false;

        static BannerAdsPosition currentPosition;

        public static bool IsShowing { get; private set; } = false;

        public static int ActiveBannerAdsListId
        {
            get
            {
                if (activeBannerAds == null)
                {
                    return -1;
                }
                else
                {
                    return activeBannerAds.ListId;
                }
            }
        }

        public static string ActiveBannerAdsId
        {
            get
            {
                if (activeBannerAds == null)
                {
                    return "";
                }
                else
                {
                    return activeBannerAds.AdId;
                }
            }
        }

        static void Init()
        {
            ///
            TryGetBannerIfNeeded();

            ///
            inited = true;
        }

        public static void TryInit()
        {
            if (!inited)
            {
                Init();
            }
        }

        static BannerAds()
        {
            TryInit();
        }

        public static void Show(BannerAdsPosition bannerAdsPosition)
        {
            ///
            IsShowing = true;
            currentPosition = bannerAdsPosition;

            ///
            if (!Ads.EnableInterAndBanner)
            {
                return;
            }

            ///
            TryGetBannerIfNeeded();

            ///
            if (activeBannerAds != null)
            {
                activeBannerAds.Show(bannerAdsPosition);
            }

            ///
            AdsAnalytics.LogShowBanner(bannerAdsPosition);
        }

        public static void Hide()
        {
            ///
            IsShowing = false;

            ///
            if (!Ads.EnableInterAndBanner)
            {
                return;
            }

            ///
            TryGetBannerIfNeeded();

            ///
            if (activeBannerAds != null)
            {
                activeBannerAds.Hide();
            }
        }

        static void TryGetBannerIfNeeded()
        {
            ///
            if (!Ads.EnableInterAndBanner)
            {
                return;
            }

            ///
            if (allBannerAds == null && AppodealAds.Instance != null /*AdmobAds.Instance != null*/)
            {
                ///
                allBannerAds = new List<IBannerAds>();

                // bannerAds = AdmobAds.Instance.Banner;
                AddBannerAdsToList(AppodealAds.Instance.Banner);

                ///
                RelocateActiveBannerAds();
            }
        }

        static void AddBannerAdsToList(IBannerAds bannerAds)
        {
            ///
            int listId = allBannerAds.Count;

            ///
            bannerAds.ListId = listId;

            ///
            bannerAds.OnAvailabilityChanged += OnAdsAvailabilityChanged;

            ///
            allBannerAds.Add(bannerAds);
        }

        static void OnAdsAvailabilityChanged(IBannerAds bannerAds)
        {
            UnityThreadHelper.Instance.DispatchToUnityThread
                (
                () =>
                {
                    RelocateActiveBannerAds();
                }
                );
        }

        static void RelocateActiveBannerAds()
        {
            ///
            if (allBannerAds == null)
            {
                return;
            }

            ///
            bool foundAvailableBanner = false;
            for (int i = 0; i < allBannerAds.Count; i++)
            {
                ///
                var ads = allBannerAds[i];

                ///
                if (ads.IsAvailable)
                {
                    SetActiveBannerAds(ads);
                    foundAvailableBanner = true;
                    break;
                }
            }

            ///
            if (!foundAvailableBanner)
            {
                SetActiveBannerAds(null);
            }
        }

        static void SetActiveBannerAds(IBannerAds bannerAds)
        {
            ///
            if (bannerAds == activeBannerAds)
            {
                return;
            }

            ///            
            activeBannerAds?.Hide();

            ///
            activeBannerAds = bannerAds;

            ///
            if (IsShowing)
            {
                activeBannerAds?.Show(currentPosition);
            }
            else
            {
                activeBannerAds?.Hide();
            }
        }
    }

}