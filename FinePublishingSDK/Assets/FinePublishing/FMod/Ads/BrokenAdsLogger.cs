﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FMod
{
    public class BrokenAdsLogger : MonoBehaviour
    {
        const string InterstitialAdsCountKey = "BrokenLogger_Inter";
        const string RewardedAdsCountKey = "BrokenLogger_Rewarded";

        int currentInterAdsCount = 0;
        int currentRewardedAdsCount = 0;

        public void Start()
        {
            ///
            LoadAdsCount();

            TryLogBrokenAds();

            ///
            InterstitialAds.OnShowAds += OnShowedInterAds;
            InterstitialAds.OnClosedAds += OnClosedInterAds;
            RewardedAds.OnShowAds += OnShowedRewarded;
            RewardedAds.OnClosedAds += OnClosedRewarded;
        }

        private void TryLogBrokenAds(string suffix = "")
        {
            ///
            if (currentRewardedAdsCount > 0)
            {
                Analytics.LogEvent("BrokenRewardedAds" + suffix, currentRewardedAdsCount);
            }
            if (currentInterAdsCount > 0)
            {
                Analytics.LogEvent("BrokenInterAds" + suffix, currentInterAdsCount);
            }

            ///
            ResetAdsCount();
        }

        public void OnApplicationQuit()
        {
            ///
            TryLogBrokenAds("OnQuit");
        }

        void OnShowedInterAds()
        {
            ///
            currentInterAdsCount++;
            SaveAdsCount();
        }

        void OnClosedInterAds()
        {
            currentInterAdsCount--;
            SaveAdsCount();
        }

        void OnShowedRewarded()
        {
            ///
            currentRewardedAdsCount++;
            SaveAdsCount();
        }

        void OnClosedRewarded(bool isCompleted)
        {
            currentRewardedAdsCount--;
            SaveAdsCount();
        }

        void LoadAdsCount()
        {
            currentInterAdsCount = PlayerPrefs.GetInt(InterstitialAdsCountKey, 0);
            currentRewardedAdsCount = PlayerPrefs.GetInt(RewardedAdsCountKey, 0);
        }

        void ResetAdsCount()
        {
            ///
            currentInterAdsCount = 0;
            currentRewardedAdsCount = 0;

            ///
            PlayerPrefs.SetInt(InterstitialAdsCountKey, 0);
            PlayerPrefs.SetInt(RewardedAdsCountKey, 0);

            ///
            PlayerPrefs.Save();
        }

        void SaveAdsCount()
        {
            ///
            PlayerPrefs.SetInt(InterstitialAdsCountKey, currentInterAdsCount);
            PlayerPrefs.SetInt(RewardedAdsCountKey, currentRewardedAdsCount);

            ///
            PlayerPrefs.Save();
        }
    }

}