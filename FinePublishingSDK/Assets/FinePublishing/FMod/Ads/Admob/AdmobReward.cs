﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if USE_STANDALONE_ADMOB
using GoogleMobileAds.Api; 
#endif

namespace FMod
{
    public class AdmobReward : MonoBehaviour, IRewardedAds
    {
        public static event System.Action OnAdmobRewardedAdsClosed = delegate { };

        [SerializeField]
        MString unitId = new MString();


#if USE_STANDALONE_ADMOB
        RewardBasedVideoAd rewardBasedVideoAd;

        RewardedAdsClosedCallback onClosedCallback = null;

        public string AdId => AdsIds.AdmobRewarded;

        public bool IsAvailable
        {
            get
            {
                ///
                if (Application.isEditor)
                {
                    return false;
                }

                ///
                if (rewardBasedVideoAd == null)
                {
                    return false;
                }

                ///
                return rewardBasedVideoAd.IsLoaded();
            }
        }

        public bool IsMonetizable => true;

        void RequestNewAds()
        {
            if (rewardBasedVideoAd == null)
            {
                rewardBasedVideoAd = RewardBasedVideoAd.Instance;

                rewardBasedVideoAd.OnAdClosed += RewardBasedVideoAd_OnAdClosed;
                rewardBasedVideoAd.OnAdRewarded += RewardBasedVideoAd_OnAdRewarded;
            }

            ///
            AdRequest request = new AdRequest.Builder().Build();
            rewardBasedVideoAd.LoadAd(request, unitId);
        }

        public void Awake()
        {
            RequestNewAds();
        }

        private void RewardBasedVideoAd_OnAdRewarded(object sender, Reward e)
        {
            if (onClosedCallback != null)
            {
                onClosedCallback(true);
            }
        }

        private void RewardBasedVideoAd_OnAdClosed(object sender, System.EventArgs e)
        {
            ///
            OnAdmobRewardedAdsClosed();

            ///
            RequestNewAds();
        }

        public void Show(RewardedAdsClosedCallback onClosedCallback)
        {
            this.onClosedCallback = onClosedCallback;
            rewardBasedVideoAd.Show();
        } 
#else
        public bool IsAvailable => throw new System.NotImplementedException();

        public bool IsMonetizable => throw new System.NotImplementedException();

        public string AdId => throw new System.NotImplementedException();

        public void Show(RewardedAdsClosedCallback onClosedCallback)
        {
            throw new System.NotImplementedException();
        }

#endif
    }

}