﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FMod
{
    public class AppRatingLauncher : MonoBehaviour
    {
        public void Launch()
        {
            string ratingUrl = AppInfo.Instance.GetRatingUrl();
            if (ratingUrl != null)
            {
                Application.OpenURL(ratingUrl);
                Analytics.LogEvent("LaunchAppRating", 1);
            }
        }

    }

}