﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FMod
{
    public class ExposureAnalytics : MonoBehaviour
    {
        const float FirstDuration = 5;
        const float SecondDuration = 10;

        [SerializeField]
        string objectName;

        private void OnValidate()
        {
            UnityEngine.Assertions.Assert.IsFalse(string.IsNullOrEmpty(objectName.Trim()));
        }

        public void Awake()
        {
            objectName = objectName.Trim();
        }

        public void OnEnable()
        {
            StartCoroutine(LogLoop());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        IEnumerator LogLoop()
        {
            ///
            yield return new WaitForSeconds(FirstDuration);
            FMod.Analytics.LogEvent("Expose_" + objectName, FirstDuration);

            ///
            while (true)
            {
                yield return new WaitForSeconds(SecondDuration);
                FMod.Analytics.LogEvent("Expose_" + objectName, SecondDuration);
            }

        }
    }
}