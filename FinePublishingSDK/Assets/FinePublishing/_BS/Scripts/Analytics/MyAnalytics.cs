﻿// #define LOG_TOUCHES

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMod;
using System;

namespace FMod
{
    public class MyAnalytics : MonoBehaviour
    {
        const string InstalledTimeKey = "InstalledTime";
        const string FirstIAPInitiationKey = "FirstIAPInit";
        const string FirstIAPSuccessKey = "FirstIAPSuccess";
        const int MinutesInGameStep = 2;
        const int MinutesInGameMax = 60;
        const int MaxTouches = 5;
        const int LoadingTimeInterval = 5;

        const float DefaultIAPRevenueCutRate = 0.3f;
        const string IAPRevenueCutRateRemoteKey = "IAPRevenueCutRate";

        int touchesCount = 0;

        public static double RetentionDays
        {
            get
            {
                return (DateTime.Now.Date - InstalledTime.Date).TotalDays;
            }
        }

        public static float IAPRevenueCutRate { get; private set; }

        public static MyAnalytics Instance { get; private set; }

        public static DateTime InstalledTime
        {
            get
            {
                ///
                if (!loadedInstalledTime)
                {
                    LoadInstalledTime();
                }

                ///
                return installedTime;
            }
        }

        public static TimeSpan RealTimeSinceInstalled
        {
            get { return System.DateTime.Now - InstalledTime; }
        }

        static DateTime installedTime;
        static bool loadedInstalledTime = false;

        bool loggedLoadingTime = false;

        static int minutesInGame = 0;

        public void Awake()
        {
            ///
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            ///
            Instance = this;

            ///
            LoadInstalledTime();

            ///
            StartCoroutine(LogInGameTimeAsync());

            ///
            IAPRevenueCutRate = DefaultIAPRevenueCutRate;
            RemoteSettings.Updated += RemoteSettings_Updated;

            ///
            DontDestroyOnLoad(gameObject);
        }

        private void RemoteSettings_Updated()
        {
            IAPRevenueCutRate = RemoteSettings.GetFloat(IAPRevenueCutRateRemoteKey, DefaultIAPRevenueCutRate);
        }

        public void Start()
        {
            LogRetentionDay();
        }

        public void LateUpdate()
        {
            // Loading time
            if (!loggedLoadingTime)
            {
                ///
                Analytics.LogEvent("LoadingTime", Time.realtimeSinceStartup);

                ///
                loggedLoadingTime = true;
            }

            // Log touches
#if LOG_TOUCHES
        LogTouches(); 
#endif
        }

        void LogRetentionDay()
        {
            var retentionDays = RetentionDays;
            var parameters = new Dictionary<string, object>()
        {
            {"day", (int)retentionDays }
        };

            if (retentionDays >= 1)
            {
                Analytics.LogEvent("Retention", 1, parameters);
            }
        }

        void LogTouches()
        {
            ///
            if (touchesCount == MaxTouches)
            {
                return;
            }

            ///
            foreach (var item in Input.touches)
            {
                if (item.phase == TouchPhase.Began)
                {
                    touchesCount++;
                    var parameters = new Dictionary<string, object>()
                {
                    {"order", touchesCount }
                };
                    Analytics.LogEvent("Touch", 1, parameters);
                    break;
                }
            }
        }

        IEnumerator LogInGameTimeAsync()
        {
            while (true)
            {
                ///
                yield return new WaitForSecondsRealtime(MinutesInGameStep * 60.0f);

                ///
                LogInGameTime();
            }
        }

        static void LogInGameTime()
        {
            minutesInGame += MinutesInGameStep;
            Analytics.LogEvent("MinutesInGame", "sessionLength", minutesInGame, MinutesInGameStep);
        }

        static void LoadInstalledTime()
        {
            ///
            string installedTimeTicksString = PlayerPrefs.GetString(InstalledTimeKey, "0");
            long installedTimeTicks = long.Parse(installedTimeTicksString);


            ///
            if (installedTimeTicks == 0)
            {
                SaveInstalledTime();
                return;
            }

            ///
            installedTime = new System.DateTime(installedTimeTicks);

            ///
            loadedInstalledTime = true;
        }

        static void SaveInstalledTime()
        {
            installedTime = DateTime.Now;
            PlayerPrefs.SetString(InstalledTimeKey, DateTime.Now.Ticks.ToString());
            loadedInstalledTime = true;
        }

        public static void LogPurchaseWithTenjin(UnityEngine.Purchasing.Product product)
        {
#if UNITY_PURCHASING && !UNITY_STANDALONE
#if !UNITY_EDITOR
        ///
        InitTenjin.LogPurchase(product); 
#endif
#endif
        }
    } 
}

public enum IAPAnalyticsName
{
    RemoveAds,

}