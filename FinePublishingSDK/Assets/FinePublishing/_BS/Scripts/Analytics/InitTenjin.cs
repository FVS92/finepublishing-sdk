﻿#define USE_TENJIN_ANDROID

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class InitTenjin : MonoBehaviour
{
    const string ApiKey = "WX6ABD3DQXUG5GCTZ9SHYZNVMDFFPCDN"; // This is key for FinePublishing's partners Get it: https://www.tenjin.io/dashboard/organizations

    public static BaseTenjin TenjinInstance { get; private set; }

    static bool inited = false;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_IOS || (UNITY_ANDROID && USE_TENJIN_ANDROID)

        ///
        if (inited)
        {
            return;
        }

        TenjinInstance = Tenjin.getInstance(ApiKey);

        // Connect
        var deeplink = DeepLinkListener.LaunchingDeeplink;
        if (string.IsNullOrWhiteSpace(deeplink))
        {
            TenjinInstance.Connect();
            Debug.Log("Connected tenjin, no deep link");
        }
        else
        {
            TenjinInstance.Connect(deeplink);
            Debug.LogFormat("Connected tenjin, deep link: {0}", deeplink);
        }

        ///
        inited = true;
#endif
    }

    public static void LogPurchase(Product purchasedProduct)
    {
#if !UNITY_STANDALONE && USE_TENJIN_ANDROID
        var price = purchasedProduct.metadata.localizedPrice;
        double lPrice = decimal.ToDouble(price);
        var currencyCode = purchasedProduct.metadata.isoCurrencyCode;

        var wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(purchasedProduct.receipt);
        if (null == wrapper)
        {
            return;
        }

        var payload = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt
        var productId = purchasedProduct.definition.id;
#endif

#if UNITY_ANDROID && USE_TENJIN_ANDROID

        var gpDetails = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
        var gpJson = (string)gpDetails["json"];
        var gpSig = (string)gpDetails["signature"];

        LogAndroidPurchase(productId, currencyCode, 1, lPrice, gpJson, gpSig);

#elif UNITY_IOS

        var transactionId = purchasedProduct.transactionID;

        LogIosPurchase(productId, currencyCode, 1, lPrice, transactionId, payload);

#endif

    }

    public static void LogCompletedTutorial()
    {
#if !UNITY_STANDALONE
        TenjinInstance.SendEvent("CompletedTutorial");
#endif
    }

    public static void LogEngaged()
    {
#if !UNITY_STANDALONE
        TenjinInstance.SendEvent("Engaged");
#endif
    }

    public static void LogAchievedLevel(string level)
    {
#if !UNITY_STANDALONE
        TenjinInstance.SendEvent("AchievedLevel", level);
#endif
    }


    static void LogAndroidPurchase(string ProductId, string CurrencyCode, int Quantity, double UnitPrice, string Receipt, string Signature)
    {
#if UNITY_ANDROID && USE_TENJIN_ANDROID
        BaseTenjin instance = Tenjin.getInstance(ApiKey);
        instance.Transaction(ProductId, CurrencyCode, Quantity, UnitPrice, null, Receipt, Signature);
#endif
    }

    static void LogIosPurchase(string ProductId, string CurrencyCode, int Quantity, double UnitPrice, string TransactionId, string Receipt)
    {
#if !UNITY_STANDALONE
        BaseTenjin instance = Tenjin.getInstance(ApiKey);
        instance.Transaction(ProductId, CurrencyCode, Quantity, UnitPrice, TransactionId, Receipt, null);
#endif
    }
}
