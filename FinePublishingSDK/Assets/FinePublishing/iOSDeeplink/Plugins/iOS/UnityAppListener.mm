#include "UnityAppListener.h"
#import "DeepLinkAppDelegate.h"

static UnityAppListener *_instance = [UnityAppListener sharedInstance];

@interface UnityAppListener()
@end

@implementation UnityAppListener

#pragma mark Object Initialization



+ (UnityAppListener *)sharedInstance
{
    return _instance;
}

+ (void)initialize {
    if(!_instance) {
        _instance = [[UnityAppListener alloc] init];
    }
}

- (id)init
{
    if(_instance != nil) {
        return _instance;
    }
    
  if ((self = [super init])) {
      _instance = self;
    UnityRegisterAppDelegateListener(self);
  }
  return self;
}

#pragma mark - App (Delegate) Lifecycle

// didBecomeActive: and onOpenURL: are called by Unity's AppController
// because we implement <AppDelegateListener> and registered via UnityRegisterAppDelegateListener(...) above.

- (void)didFinishLaunching:(NSNotification *)notification
{
  // [[FBSDKApplicationDelegate sharedInstance] application:[UIApplication sharedApplication]
  //                         didFinishLaunchingWithOptions:notification.userInfo];
    
    NSURL *url = notification.userInfo[UIApplicationLaunchOptionsURLKey];
    if (url) {
        // TODO: handle URL from here
        [DeepLinkAppDelegate setLastLinkURL:url.absoluteString];
    }
    else
    {
        [DeepLinkAppDelegate setLastLinkURL:@""];
    }
}

- (void)didBecomeActive:(NSNotification *)notification
{
  
}

- (void)onOpenURL:(NSNotification *)notification
{
 
}
@end
