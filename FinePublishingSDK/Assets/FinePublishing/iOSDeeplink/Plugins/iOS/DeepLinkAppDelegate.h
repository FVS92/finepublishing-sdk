#ifndef DeepLinkAppDelegate_h
#define DeepLinkAppDelegate_h

@interface DeepLinkAppDelegate : NSObject

+ (char *) deepLinkURL;
+ (void) setLastLinkURL:(NSString*)url;
@end

#endif /* DeepLinkAppDelegate_h */