#import "DeepLinkAppDelegate.h"

extern "C" {
	char * GetDeepLinkURL();
}

char * GetDeepLinkURL() {
	return [DeepLinkAppDelegate deepLinkURL];
}