#import <UIKit/UIKit.h>

#import "AppDelegateListener.h"
#include "RegisterMonoModules.h"

@interface UnityAppListener : NSObject <AppDelegateListener>
{
  
}
+ (UnityAppListener *)sharedInstance;
@end
