#import "DeepLinkAppDelegate.h"

char* cStringCopy(const char* string);

@implementation DeepLinkAppDelegate

static NSString* _lastURL=@"";

+ (char *) deepLinkURL
{
	return cStringCopy([(_lastURL ? _lastURL : @"") UTF8String]);
}

+ (void) setLastLinkURL:(NSString*)url
{
	_lastURL = url;
}
@end

char* cStringCopy(const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}

// This takes a char* you get from Unity and converts it to an NSString* to use in your objective c code. You can mix c++ and objective c all in the same file.
static NSString* CreateNSString(const char* string)
{
    if (string != NULL)
        return [NSString stringWithUTF8String:string];
    else
        return [NSString stringWithUTF8String:""];
}