using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;

public class DeepLinkListener : MonoBehaviour
{
    const string test_Deeplink = "fb563861197279136://?campaign_id=dUaW2nLnueWXGW2VgY2p0D";

#if !UNITY_EDITOR && UNITY_IOS
    [DllImport("__Internal")]
    private static extern string GetDeepLinkURL(); 
#endif

    static bool inited = false;
    static string launchingDeeplink;

    public static string LaunchingDeeplink
    {
        get
        {
            ///
            TryInit();

            ///
            return launchingDeeplink;
        }
    }

    void Awake()
    {
        ///
        // Debug.LogFormat("Deep link: {0}", LaunchingDeeplink);
    }

    static void TryInit()
    {
        ///
        if (inited)
        {
            return;
        }

        ///
#if !UNITY_EDITOR && UNITY_IOS
        launchingDeeplink = GetDeepLinkURL(); 
#else
#if UNITY_EDITOR
        launchingDeeplink = test_Deeplink;
#else
        launchingDeeplink = "";
#endif
#endif

        ///
        inited = true;
    }
}