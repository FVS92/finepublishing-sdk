# Trước khi import FinePublishing SDK:
## Unity:
- Connect Unity services với tài khoản của publisher
- Bật Unity analytics
- Bật Unity In-app purchasing, import package
- Bật Unity Ads, bỏ chọn Enable built-in Ads extension.
## Facebook:
- Download SDK mới nhất: https://developers.facebook.com/docs/unity
## Tenjin:
- Download SDK mới nhất: https://github.com/tenjin/tenjin-unity-sdk
## Appodeal:
- Download SDK mới nhất: https://wiki.appodeal.com/en
# Import FinePublishing:
## Download:
Tải bản mới nhất ở đây: https://bitbucket.org/FVS92/finepublishing-sdk/src/master/Release

## Sau khi import:
* __Config GDPR screen:__ thiết lập scene _FinePublishing/\_BS/Scenes/GDPR_ làm scene đầu tiên của game
* __Thêm prefabs:__ kéo tất cả các prefabs (trừ BannerAds, cái này tùy chọn) ở _FinePublishing/Prefabs_ vào scene đầu tiên của game
* __Config appodeal:__ điền key cho iOS và Android vào prefab AppodealAds ở trong scene (vừa kéo ở trên)
* __Hiện quảng cáo:__
     * Banner: dùng prefab BannerAds ở _FinePublishing/Prefabs_ để luôn hiện banner. Hoặc dùng script _FMod.BannerAds.Show(bannerAdsPosition)_ và _FMod.BannerAds.Hide()_ 
     * Inter: dùng _FMod.InterstitialAds.IsAvailable_ để check, _FMod.InterstitialAds.Show(callback)_ để show
     * Rewarded: dùng _FMod.RewardedAds.IsAvailable_ để check, _FMod.RewardedAds.Show(callback)_ để show
     
_Lưu ý:_ luôn gọi hiện show interstitial ads mọi lúc có thể (sau khi check available), tần suất hiện ads để  SDK lo.

* __Gỡ quảng cáo:__ _FMod.Ads.EnableInterAndBanner = false_
* __Log purchase:__ log ngay sau khi người chơi mua thành công IAP item nào đó. _FMod.MyAnalytics.LogPurchaseWithTenjin(product)_
* __[OPTIONAL] Use iOS native rating request:__ hiện bảng rating của iOS (người chơi có thể rate luôn trong game): _success = NativeReviewRequest.RequestReview()_ . Nếu trả về false nghĩa là phiên bản iOS không hỗ trợ, phải hiện bảng rating tự làm trong game.